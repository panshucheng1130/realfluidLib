#include "heos.h"

#include <cstring>
#include <iostream>

#include "my_macro.h"
#include "global_function.h"

namespace HEOS
{

void nDodecane::Initialization(const Species &species_param)
{

    strcpy(species_.name, species_param.name);
    strcpy(species_.molecular_formula, species_param.molecular_formula);
    species_.mass_molar = species_param.mass_molar;
    species_.omega = species_param.omega;
    species_.p_c = species_param.p_c;
    species_.T_c = species_param.T_c;
    species_.rho_c_molar = species_param.rho_c_molar;
    species_.rho_c_mass = species_param.rho_c_molar * species_param.mass_molar;
    Loop1D(i, 0, 4)
    {
        species_.ck[i] = species_param.ck[i];
        species_.uk[i] = species_param.uk[i];
    }
    species_.uk[0] = NAN;

    Loop1D(i, 0, 11) species_.nk[i] = species_param.nk[i];

    species_.entropy_reference = species_param.entropy_reference;
    species_.enthalpy_reference = species_param.enthalpy_reference;

    species_.a1 = (-species_.entropy_reference / R_UNIVERSAL * species_.mass_molar - 1.0);
    species_.a2 = species_.enthalpy_reference / R_UNIVERSAL * species_.mass_molar / species_.T_c;
}

double nDodecane::HelmholtzIdeal(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;

    double alpha_0;

    alpha_0 = species_.a1 + species_.a2 * tau + log(delta) + (species_.ck[0] - 0.10e1) * log(tau) +
              species_.ck[1] * log(0.10e1 - exp(-species_.uk[1] * tau / species_.T_c)) +
              species_.ck[2] * log(0.10e1 - exp(-species_.uk[2] * tau / species_.T_c)) +
              species_.ck[3] * log(0.10e1 - exp(-species_.uk[3] * tau / species_.T_c)) +
              species_.ck[4] * log(0.10e1 - exp(-species_.uk[4] * tau / species_.T_c));

    return alpha_0;
}

double nDodecane::HelmholtzResidual(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;

    double alpha_r;
    alpha_r = species_.nk[0] * delta * pow(tau, 0.32e0) + species_.nk[1] * delta * pow(tau, 0.123e1) +
              species_.nk[2] * delta * pow(tau, 0.15e1) + species_.nk[3] * delta * delta * pow(tau, 0.14e1) +
              species_.nk[4] * pow(delta, 0.3e1) * pow(tau, 0.7e-1) + species_.nk[5] * pow(delta, 0.7e1) * pow(tau, 0.8e0) +
              species_.nk[6] * delta * delta * pow(tau, 0.216e1) * exp(-delta) +
              species_.nk[7] * pow(delta, 0.5e1) * pow(tau, 0.11e1) * exp(-delta) +
              species_.nk[8] * delta * pow(tau, 0.41e1) * exp(-delta * delta) +
              species_.nk[9] * pow(delta, 0.4e1) * pow(tau, 0.56e1) * exp(-delta * delta) +
              species_.nk[10] * pow(delta, 0.3e1) * pow(tau, 0.145e2) * exp(-pow(delta, 0.3e1)) +
              species_.nk[11] / pow(delta, 0.4e1) * pow(tau, 0.120e2) * exp(-pow(delta, 0.3e1));

    return alpha_r;
}

double nDodecane::dalpha0_ddelta_tau(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;

    double dalpha0_ddelta_tau;
    dalpha0_ddelta_tau = 1.0 / delta;

    return dalpha0_ddelta_tau;
}

double nDodecane::dalpha0_dtau_delta(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;
    double T_c = species_.T_c;

    double dalpha0_dtau_delta;
    dalpha0_dtau_delta = species_.a2 + (species_.ck[0] - 0.10e1) / tau +
                         species_.ck[1] * species_.uk[1] / T_c * exp(-species_.uk[1] * tau / T_c) / (0.10e1 - exp(-species_.uk[1] * tau / T_c)) +
                         species_.ck[2] * species_.uk[2] / T_c * exp(-species_.uk[2] * tau / T_c) / (0.10e1 - exp(-species_.uk[2] * tau / T_c)) +
                         species_.ck[3] * species_.uk[3] / T_c * exp(-species_.uk[3] * tau / T_c) / (0.10e1 - exp(-species_.uk[3] * tau / T_c)) +
                         species_.ck[4] * species_.uk[4] / T_c * exp(-species_.uk[4] * tau / T_c) / (0.10e1 - exp(-species_.uk[4] * tau / T_c));

    return dalpha0_dtau_delta;
}

double nDodecane::dalphaR_ddelta_tau(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;

    double dalphaR_ddelta_tau;
    dalphaR_ddelta_tau = species_.nk[0] * pow(tau, 0.32e0) + species_.nk[1] * pow(tau, 0.123e1) +
                         species_.nk[2] * pow(tau, 0.15e1) + 0.2e1 * species_.nk[3] * delta * pow(tau, 0.14e1) +
                         0.3e1 * species_.nk[4] * delta * delta * pow(tau, 0.7e-1) +
                         0.7e1 * species_.nk[5] * pow(delta, 0.6e1) * pow(tau, 0.8e0) +
                         0.2e1 * species_.nk[6] * delta * pow(tau, 0.216e1) * exp(-delta) -
                         species_.nk[6] * delta * delta * pow(tau, 0.216e1) * exp(-delta) +
                         0.5e1 * species_.nk[7] * pow(delta, 0.4e1) * pow(tau, 0.11e1) * exp(-delta) -
                         species_.nk[7] * pow(delta, 0.5e1) * pow(tau, 0.11e1) * exp(-delta) +
                         species_.nk[8] * pow(tau, 0.41e1) * exp(-delta * delta) -
                         0.2e1 * species_.nk[8] * delta * delta * pow(tau, 0.41e1) * exp(-delta * delta) +
                         0.4e1 * species_.nk[9] * pow(delta, 0.3e1) * pow(tau, 0.56e1) * exp(-delta * delta) -
                         0.2e1 * species_.nk[9] * pow(delta, 0.5e1) * pow(tau, 0.56e1) * exp(-delta * delta) +
                         0.3e1 * species_.nk[10] * delta * delta * pow(tau, 0.145e2) * exp(-pow(delta, 0.3e1)) -
                         0.3e1 * species_.nk[10] * pow(delta, 0.5e1) * pow(tau, 0.145e2) * exp(-pow(delta, 0.3e1)) +
                         0.4e1 * species_.nk[11] * pow(delta, 0.3e1) * pow(tau, 0.120e2) * exp(-pow(delta, 0.3e1)) -
                         0.3e1 * species_.nk[11] * pow(delta, 0.6e1) * pow(tau, 0.120e2) * exp(-pow(delta, 0.3e1));

    return dalphaR_ddelta_tau;
}

double nDodecane::dalphaR_dtau_delta(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;

    double dalphaR_dtau_delta;
    dalphaR_dtau_delta = 0.32e0 * species_.nk[0] * delta * pow(tau, -0.68e0) + 0.123e1 * species_.nk[1] * delta * pow(tau, 0.23e0) +
                         0.15e1 * species_.nk[2] * delta * pow(tau, 0.5e0) + 0.14e1 * species_.nk[3] * delta * delta * pow(tau, 0.4e0) +
                         0.7e-1 * species_.nk[4] * pow(delta, 0.3e1) * pow(tau, -0.93e0) +
                         0.8e0 * species_.nk[5] * pow(delta, 0.7e1) * pow(tau, -0.2e0) +
                         0.216e1 * species_.nk[6] * delta * delta * pow(tau, 0.116e1) * exp(-delta) +
                         0.11e1 * species_.nk[7] * pow(delta, 0.5e1) * pow(tau, 0.1e0) * exp(-delta) +
                         0.41e1 * species_.nk[8] * delta * pow(tau, 0.31e1) * exp(-delta * delta) +
                         0.56e1 * species_.nk[9] * pow(delta, 0.4e1) * pow(tau, 0.46e1) * exp(-delta * delta) +
                         0.145e2 * species_.nk[10] * pow(delta, 0.3e1) * pow(tau, 0.135e2) * exp(-pow(delta, 0.3e1)) +
                         0.120e2 * species_.nk[11] * pow(delta, 0.4e1) * pow(tau, 0.110e2) * exp(-pow(delta, 0.3e1));

    return dalphaR_dtau_delta;
}

double nDodecane::ddalpha0_dddelta_tau(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;

    double ddalpha0_dddelta_tau;
    ddalpha0_dddelta_tau = -1.0 / delta / delta;

    return ddalpha0_dddelta_tau;
}

double nDodecane::ddalpha0_ddtau_delta(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;
    double T_c = species_.T_c;

    double ddalpha0_ddtau_delta;
    ddalpha0_ddtau_delta = -(species_.ck[0] - 0.10e1) * pow(tau, -0.2e1) -
                           species_.ck[1] * species_.uk[1] * species_.uk[1] * pow(T_c, -0.2e1) *
                               exp(-species_.uk[1] * tau / T_c) / (0.10e1 - exp(-species_.uk[1] * tau / T_c)) -
                           species_.ck[1] * species_.uk[1] * species_.uk[1] * pow(T_c, -0.2e1) *
                               pow(exp(-species_.uk[1] * tau / T_c), 0.2e1) * pow(0.10e1 - exp(-species_.uk[1] * tau / T_c), -0.2e1) -
                           species_.ck[2] * species_.uk[2] * species_.uk[2] *
                               pow(T_c, -0.2e1) * exp(-species_.uk[2] * tau / T_c) / (0.10e1 - exp(-species_.uk[2] * tau / T_c)) -
                           species_.ck[2] * species_.uk[2] * species_.uk[2] *
                               pow(T_c, -0.2e1) * pow(exp(-species_.uk[2] * tau / T_c), 0.2e1) * pow(0.10e1 - exp(-species_.uk[2] * tau / T_c), -0.2e1) -
                           species_.ck[3] * species_.uk[3] * species_.uk[3] *
                               pow(T_c, -0.2e1) * exp(-species_.uk[3] * tau / T_c) / (0.10e1 - exp(-species_.uk[3] * tau / T_c)) -
                           species_.ck[3] * species_.uk[3] * species_.uk[3] *
                               pow(T_c, -0.2e1) * pow(exp(-species_.uk[3] * tau / T_c), 0.2e1) * pow(0.10e1 - exp(-species_.uk[3] * tau / T_c), -0.2e1) -
                           species_.ck[4] * species_.uk[4] * species_.uk[4] *
                               pow(T_c, -0.2e1) * exp(-species_.uk[4] * tau / T_c) / (0.10e1 - exp(-species_.uk[4] * tau / T_c)) -
                           species_.ck[4] * species_.uk[4] * species_.uk[4] *
                               pow(T_c, -0.2e1) * pow(exp(-species_.uk[4] * tau / T_c), 0.2e1) * pow(0.10e1 - exp(-species_.uk[4] * tau / T_c), -0.2e1);

    return ddalpha0_ddtau_delta;
}

double nDodecane::ddalphaR_dddelta_tau(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;

    double ddalphaR_dddelta_tau;
    ddalphaR_dddelta_tau = species_.nk[6] * delta * delta * pow(tau, 0.216e1) * exp(-delta) +
                           species_.nk[7] * pow(delta, 0.5e1) * pow(tau, 0.11e1) * exp(-delta) -
                           0.6e1 * species_.nk[8] * delta * pow(tau, 0.41e1) * exp(-delta * delta) -
                           0.18e2 * species_.nk[9] * pow(delta, 0.4e1) * pow(tau, 0.56e1) * exp(-delta * delta) -
                           0.4e1 * species_.nk[6] * delta * pow(tau, 0.216e1) * exp(-delta) +
                           0.20e2 * species_.nk[7] * pow(delta, 0.3e1) * pow(tau, 0.11e1) * exp(-delta) +
                           0.12e2 * species_.nk[9] * delta * delta * pow(tau, 0.56e1) * exp(-delta * delta) -
                           0.10e2 * species_.nk[7] * pow(delta, 0.4e1) * pow(tau, 0.11e1) * exp(-delta) +
                           0.2e1 * species_.nk[6] * pow(tau, 0.216e1) * exp(-delta) +
                           0.4e1 * species_.nk[8] * pow(delta, 0.3e1) * pow(tau, 0.41e1) * exp(-delta * delta) +
                           0.2e1 * species_.nk[3] * pow(tau, 0.14e1) + 0.6e1 * species_.nk[4] * delta * pow(tau, 0.7e-1) +
                           0.42e2 * species_.nk[5] * pow(delta, 0.5e1) * pow(tau, 0.8e0) +
                           0.6e1 * species_.nk[10] * delta * pow(tau, 0.145e2) * exp(-pow(delta, 0.3e1)) +
                           0.12e2 * species_.nk[11] * delta * delta * pow(tau, 0.120e2) * exp(-pow(delta, 0.3e1)) +
                           0.4e1 * species_.nk[9] * pow(delta, 0.6e1) * pow(tau, 0.56e1) * exp(-delta * delta) -
                           0.24e2 * species_.nk[10] * pow(delta, 0.4e1) * pow(tau, 0.145e2) * exp(-pow(delta, 0.3e1)) +
                           0.9e1 * species_.nk[10] * pow(delta, 0.7e1) * pow(tau, 0.145e2) * exp(-pow(delta, 0.3e1)) -
                           0.30e2 * species_.nk[11] * pow(delta, 0.5e1) * pow(tau, 0.120e2) * exp(-pow(delta, 0.3e1)) +
                           0.9e1 * species_.nk[11] * pow(delta, 0.8e1) * pow(tau, 0.120e2) * exp(-pow(delta, 0.3e1));

    return ddalphaR_dddelta_tau;
}

double nDodecane::ddalphaR_ddtau_delta(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;

    double ddalphaR_ddtau_delta;
    ddalphaR_ddtau_delta = -0.2176e0 * species_.nk[0] * delta * pow(tau, -0.168e1) +
                           0.2829e0 * species_.nk[1] * delta * pow(tau, -0.77e0) + 0.75e0 * species_.nk[2] * delta * pow(tau, -0.5e0) +
                           0.56e0 * species_.nk[3] * delta * delta * pow(tau, -0.6e0) -
                           0.651e-1 * species_.nk[4] * pow(delta, 0.3e1) * pow(tau, -0.193e1) -
                           0.16e0 * species_.nk[5] * pow(delta, 0.7e1) * pow(tau, -0.12e1) +
                           0.25056e1 * species_.nk[6] * delta * delta * pow(tau, 0.16e0) * exp(-delta) +
                           0.11e0 * species_.nk[7] * pow(delta, 0.5e1) * pow(tau, -0.9e0) * exp(-delta) +
                           0.1271e2 * species_.nk[8] * delta * pow(tau, 0.21e1) * exp(-delta * delta) +
                           0.2576e2 * species_.nk[9] * pow(delta, 0.4e1) * pow(tau, 0.36e1) * exp(-delta * delta) +
                           0.19575e3 * species_.nk[10] * pow(delta, 0.3e1) * pow(tau, 0.125e2) * exp(-pow(delta, 0.3e1)) +
                           0.13200e3 * species_.nk[11] * pow(delta, 0.4e1) * pow(tau, 0.100e2) * exp(-pow(delta, 0.3e1));

    return ddalphaR_ddtau_delta;
}

double nDodecane::ddalphaR_ddeltadtau(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;

    double ddalphaR_ddeltadtau;
    ddalphaR_ddeltadtau = 0.32e0 * species_.nk[0] * pow(tau, -0.68e0) + 0.123e1 * pow(tau, 0.23e0) * species_.nk[1] +
                          0.15e1 * species_.nk[2] * pow(tau, 0.5e0) + 0.28e1 * species_.nk[3] * delta * pow(tau, 0.4e0) +
                          0.21e0 * species_.nk[4] * delta * delta * pow(tau, -0.93e0) +
                          0.56e1 * species_.nk[5] * pow(delta, 0.6e1) * pow(tau, -0.2e0) +
                          0.432e1 * species_.nk[6] * delta * pow(tau, 0.116e1) * exp(-delta) -
                          0.216e1 * species_.nk[6] * delta * delta * pow(tau, 0.116e1) * exp(-delta) +
                          0.55e1 * species_.nk[7] * pow(delta, 0.4e1) * pow(tau, 0.1e0) * exp(-delta) -
                          0.11e1 * species_.nk[7] * pow(delta, 0.5e1) * pow(tau, 0.1e0) * exp(-delta) +
                          0.41e1 * species_.nk[8] * pow(tau, 0.31e1) * exp(-delta * delta) -
                          0.82e1 * species_.nk[8] * delta * delta * pow(tau, 0.31e1) * exp(-delta * delta) +
                          0.224e2 * species_.nk[9] * pow(delta, 0.3e1) * pow(tau, 0.46e1) * exp(-delta * delta) -
                          0.112e2 * species_.nk[9] * pow(delta, 0.5e1) * pow(tau, 0.46e1) * exp(-delta * delta) +
                          0.435e2 * species_.nk[10] * delta * delta * pow(tau, 0.135e2) * exp(-pow(delta, 0.3e1)) -
                          0.435e2 * species_.nk[10] * pow(delta, 0.5e1) * pow(tau, 0.135e2) * exp(-pow(delta, 0.3e1)) +
                          0.480e2 * species_.nk[11] * pow(delta, 0.3e1) * pow(tau, 0.110e2) * exp(-pow(delta, 0.3e1)) -
                          0.360e2 * species_.nk[11] * pow(delta, 0.6e1) * pow(tau, 0.110e2) * exp(-pow(delta, 0.3e1));

    return ddalphaR_ddeltadtau;
}

double nDodecane::dp_dT_rho(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;

    double DalpharDdelta = dalphaR_ddelta_tau(rho, T);
    double DDalpharDdeltaDtau = ddalphaR_ddeltadtau(rho, T);

    double dp_dT_rho;
    dp_dT_rho = rho * R_UNIVERSAL / species_.mass_molar + rho * R_UNIVERSAL / species_.mass_molar *
                                                              (delta * DalpharDdelta - tau * delta * DDalpharDdeltaDtau);

    return dp_dT_rho;
}

double nDodecane::dp_drho_T(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;

    double DalpharDdelta = dalphaR_ddelta_tau(rho, T);
    double DDalpharDDdelta = ddalphaR_dddelta_tau(rho, T);

    double dp_drho_T;
    dp_drho_T = R_UNIVERSAL / species_.mass_molar * T + 2 * delta * R_UNIVERSAL / species_.mass_molar * T * DalpharDdelta +
                delta * delta * R_UNIVERSAL / species_.mass_molar * T * DDalpharDDdelta;

    return dp_drho_T;
}

double nDodecane::dp_drho_e(double p, double rho, double T)
{
    double dpdT_rho = dp_dT_rho(rho, T);
    double dpdrho_T = dp_drho_T(rho, T);

    double drhodT_p = -dpdT_rho / dpdrho_T;
    double cp = HeatCapacity_P(rho, T);
    double cv = HeatCapacity_V(rho, T);

    double dpdrho_e = dpdT_rho / cv * (-cp / drhodT_p * rho * rho - p) / rho / rho;

    return dpdrho_e;
}

double nDodecane::dp_de_rho(double rho, double T)
{
    double dpdT_rho = dp_dT_rho(rho, T);
    double cv = HeatCapacity_V(rho, T);

    double dpde_rho = 1.0 / cv * dpdT_rho;

    return dpde_rho;
}

double nDodecane::Pressure(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;
    double DalpharDdelta = dalphaR_ddelta_tau(rho, T);

    double p;
    p = rho * R_UNIVERSAL / species_.mass_molar * T * (1.0 + delta * DalpharDdelta);

    return p;
}

double nDodecane::Enthalpy(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;
    double DalpharDdelta = dalphaR_ddelta_tau(rho, T);
    double DalpharDtau = dalphaR_dtau_delta(rho, T);
    double Dalpha0Dtau = dalpha0_dtau_delta(rho, T);

    double h;
    h = tau * (Dalpha0Dtau + DalpharDtau) + delta * DalpharDdelta + 1.0;
    h *= R_UNIVERSAL / species_.mass_molar * T;

    return h;
}

double nDodecane::Entropy(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;
    double DalpharDtau = dalphaR_dtau_delta(rho, T);
    double Dalpha0Dtau = dalpha0_dtau_delta(rho, T);
    double alpha0 = HelmholtzIdeal(rho, T);
    double alphar = HelmholtzResidual(rho, T);

    double s;
    s = tau * (Dalpha0Dtau + DalpharDtau) - alpha0 - alphar;
    s *= R_UNIVERSAL / species_.mass_molar;

    return s;
}

double nDodecane::HeatCapacity_V(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;
    double DDalpharDDtau = ddalphaR_ddtau_delta(rho, T);
    double DDalpha0DDtau = ddalpha0_ddtau_delta(rho, T);

    double cv;
    cv = -tau * tau * (DDalpha0DDtau + DDalpharDDtau);
    cv *= R_UNIVERSAL / species_.mass_molar;

    return cv;
}

double nDodecane::HeatCapacity_P(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;
    double cv = HeatCapacity_V(rho, T);
    double DalpharDdelta = dalphaR_ddelta_tau(rho, T);
    double DDalpharDdeltaDtau = ddalphaR_ddeltadtau(rho, T);
    double DDalpharDDdelta = ddalphaR_dddelta_tau(rho, T);

    double cp;
    cp = (1.0 + delta * DalpharDdelta - delta * tau * DDalpharDdeltaDtau) *
         (1.0 + delta * DalpharDdelta - delta * tau * DDalpharDdeltaDtau) / (1 + 2 * delta * DalpharDdelta + delta * delta * DDalpharDDdelta);
    cp = cp * R_UNIVERSAL / species_.mass_molar + cv;

    return cp;
}

double nDodecane::SpecificHeatRatio(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;
    double cv = HeatCapacity_V(rho, T);
    double cp = HeatCapacity_P(rho, T);

    double gamma = cp / cv;

    return gamma;
}

double nDodecane::SpeedOfSound(double rho, double T)
{
    double delta = rho / species_.rho_c_mass;
    double tau = species_.T_c / T;

    double DalpharDdelta = dalphaR_ddelta_tau(rho, T);
    double DDalpharDdeltaDtau = ddalphaR_ddeltadtau(rho, T);
    double DDalpharDDdelta = ddalphaR_dddelta_tau(rho, T);
    double DDalpharDDtau = ddalphaR_ddtau_delta(rho, T);
    double DDalpha0DDtau = ddalpha0_ddtau_delta(rho, T);

    double c;

    c = 1.0 + 2 * delta * DalpharDdelta + delta * delta * DDalpharDDdelta -
        (1.0 + delta * DalpharDdelta - delta * tau * DDalpharDdeltaDtau) *
            (1.0 + delta * DalpharDdelta - delta * tau * DDalpharDdeltaDtau) / tau / tau / (DDalpha0DDtau + DDalpharDDtau);

    c *= R_UNIVERSAL / species_.mass_molar * T;

    c = sqrt(c);

    return c;
}

double nDodecane::GibbsEnergy(double rho, double T)
{
    double h = Enthalpy(rho, T);
    double s = Entropy(rho, T);

    double g = h - T * s;

    return g;
}

double nDodecane::InternalEnergy(double p, double rho, double T)
{
    double h = Enthalpy(rho, T);
    double e = h - p / rho;

    return e;
}

double nDodecane::GetTempFromDenAndPress(double rho, double p, double T_guess)
{
    const int iter_max = 20;
    int iter = 0;

    const double threshold = 1.0e-6;
    const double relax_factor = 1.0e-5;
    const double T_min = 50.0;
    const double T_max = 4000.0;
    double p_iter = 0.0;
    double T_iter = 0.0;
    double e_iter = 0.0;
    double residual = 0.0;

    T_iter = T_guess;
    do
    {

        if (iter > iter_max)
        {
            std::cout << "Newtown iteration for temperature failled@!" << std::endl;
            exit(0);
        }

        if (T_iter < T_min || T_iter > T_max)
        {
            std::cout << "Temperature is beyond the boundary during the newton iteration!" << std::endl;
            exit(0);
        }

        if (std::isnan(T_iter) || std::isinf(T_iter))
        {
            std::cout << "Some terribly bad things happened in the process of newton iteration!" << std::endl;
            std::cout << "The value of emperature is not a number!" << std::endl;
            exit(0);
        }
        ++iter;
        p_iter = Pressure(rho, T_iter);

        double p_norm = fabs(p) > 1.0 ? p : 1.0;
        double dp = p - p_iter;

        residual = fabs(dp / p_norm);

        double dpdT_rho = dp_dT_rho(rho, T_iter);
        double dT = dp / dpdT_rho;
        T_iter = T_iter + dT / (1 + fabs(relax_factor * dT));

    } while (residual > threshold);

    std::cout << "Converge after " << iter << " times iterations! " << std::endl;

    return T_iter;
}

double nDodecane::GetTempFromDenAndInternalEnergy(double rho, double e, double T_guess)
{
    const int iter_max = 20;
    int iter = 0;

    const double threshold = 1.0e-6;
    const double relax_factor = 1.0e-5;
    const double T_min = 50.0;
    const double T_max = 4000.0;
    double p_iter = 0.0;
    double T_iter = 0.0;
    double e_iter = 0.0;
    double residual = 0.0;

    T_iter = T_guess;
    do
    {

        if (iter > iter_max)
        {
            std::cout << "Newtown iteration for temperature failled@!" << std::endl;
            exit(0);
        }

        if (T_iter < T_min || T_iter > T_max)
        {
            std::cout << "Temperature is beyond the boundary during the newton iteration!" << std::endl;
            exit(0);
        }

        if (std::isnan(T_iter) || std::isinf(T_iter))
        {
            std::cout << "Some terribly bad things happened in the process of newton iteration!" << std::endl;
            std::cout << "The value of emperature is not a number!" << std::endl;
            exit(0);
        }
        ++iter;
        e_iter = InternalEnergy(p_iter, rho, T_iter);

        double e_norm = fabs(e) > 1.0 ? e : 1.0;
        double de = e - e_iter;

        residual = fabs(de / e_norm);

        double cv = HeatCapacity_V(rho, T_iter);
        double dT = de / cv;
        T_iter = T_iter + dT / (1 + fabs(relax_factor * dT));

    } while (residual > threshold);

    return T_iter;
}

double nDodecane::GetDenFromTempAndPress(double T, double p, double rho_guess)
{
    const int iter_max = 20;
    int iter = 0;

    const double threshold = 1.0e-6;
    const double relax_factor = 1.0e-5;
    const double rho_min = 0.0;
    const double rho_max = 4000.0;
    double p_iter = 0.0;
    double rho_iter = 0.0;
    double e_iter = 0.0;
    double residual = 0.0;

    rho_iter = rho_guess;
    do
    {

        if (iter > iter_max)
        {
            std::cout << "Newtown iteration for density failled@!" << std::endl;
            exit(0);
        }

        if (rho_iter < rho_min || rho_iter > rho_max)
        {
            std::cout << "Density is beyond the boundary during the newton iteration!" << std::endl;
            exit(0);
        }

        if (std::isnan(rho_iter) || std::isinf(rho_iter))
        {
            std::cout << "Some terribly bad things happened in the process of newton iteration!" << std::endl;
            std::cout << "The value of density is not a number!" << std::endl;
            exit(0);
        }
        ++iter;
        p_iter = Pressure(rho_iter, T);

        double p_norm = fabs(p) > 1.0 ? p : 1.0;
        double dp = p - p_iter;

        residual = fabs(dp / p_norm);

        double dpdrho_T = dp_drho_T(rho_iter, T);
        double drho = dp / dpdrho_T;
        rho_iter = rho_iter + drho / (1 + fabs(relax_factor * drho));

    } while (residual > threshold);

    std::cout << "Converge after " << iter << " times iterations! " << std::endl;

    return rho_iter;
}

double nDodecane::GetThermoFromTemperatureAndDensity(double T, double rho, ThermodynamicProperty which_property)
{

    double thermo_property = 0.0;
    double p = Pressure(rho, T);

    switch (which_property)
    {
    case TEMPERATURE:
        std::cout << "WARNING!!!" << std::endl;
        std::cout << "ALREADY KNOWN PROPERTY: TEMPERATURE" << std::endl;
        thermo_property = T;
        break;

    case PRESSURE:
        thermo_property = p;
        break;

    case DENSITY:
        std::cout << "WARNING!!!" << std::endl;
        std::cout << "ALREADY KNOWN PROPERTY: DENSITY" << std::endl;
        thermo_property = rho;
        break;

    case VOLUME:
        std::cout << "WARNING!!!" << std::endl;
        std::cout << "THIS EOS SHOULD NOT BE USED TO CALCULATE VOLUME" << std::endl;
        thermo_property = 1.0 / rho;
        break;

    case INTERNAL_ENERGY:
        thermo_property = InternalEnergy(p, rho, T);
        break;

    case ENTHALPY:
        thermo_property = Enthalpy(rho, T);
        break;

    case ENTROPY:
        thermo_property = Entropy(rho, T);
        break;

    case GIBBS_ENERGY:
        thermo_property = GibbsEnergy(rho, T);
        break;

    case HEATCAPACITY_V:
        thermo_property = HeatCapacity_V(rho, T);
        break;

    case HEATCAPACITY_P:
        thermo_property = HeatCapacity_P(rho, T);
        break;

    case SPECIFIC_HEAT_RATIO:
        thermo_property = SpecificHeatRatio(rho, T);
        break;

    case SPEED_OF_SOUND:
        thermo_property = SpeedOfSound(rho, T);
        break;

    default:
        break;
    }

    return thermo_property;
}

double nDodecane::GetThermoFromDensityAndInternalEnergy(double rho, double e, double T_guess, ThermodynamicProperty which_property)
{

    double thermo_property = 0.0;
    double T = GetTempFromDenAndInternalEnergy(rho, e, T_guess);
    double p = Pressure(rho, T);

    switch (which_property)
    {
    case TEMPERATURE:
        thermo_property = T;
        break;

    case PRESSURE:
        thermo_property = p;
        break;

    case DENSITY:
        std::cout << "WARNING!!!" << std::endl;
        std::cout << "ALREADY KNOWN PROPERTY: DENSITY" << std::endl;
        thermo_property = rho;
        break;

    case VOLUME:
        std::cout << "WARNING!!!" << std::endl;
        std::cout << "THIS EOS SHOULD NOT BE USED TO CALCULATE VOLUME" << std::endl;
        thermo_property = 1.0 / rho;
        break;

    case INTERNAL_ENERGY:
        std::cout << "WARNING!!!" << std::endl;
        std::cout << "ALREADY KNOWN PROPERTY: INTERNAL ENERGY" << std::endl;
        thermo_property = InternalEnergy(p, rho, T);
        break;

    case ENTHALPY:
        thermo_property = Enthalpy(rho, T);
        break;

    case ENTROPY:
        thermo_property = Entropy(rho, T);
        break;

    case GIBBS_ENERGY:
        thermo_property = GibbsEnergy(rho, T);
        break;

    case HEATCAPACITY_V:
        thermo_property = HeatCapacity_V(rho, T);
        break;

    case HEATCAPACITY_P:
        thermo_property = HeatCapacity_P(rho, T);
        break;

    case SPECIFIC_HEAT_RATIO:
        thermo_property = SpecificHeatRatio(rho, T);
        break;

    case SPEED_OF_SOUND:
        thermo_property = SpeedOfSound(rho, T);
        break;

    default:
        break;
    }

    return thermo_property;
}

} // namespace HEOS