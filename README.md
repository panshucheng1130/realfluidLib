# RealFluidLIB

This is an open-source Thermodynamics properties and EOS library for real fluids. It includes cubic EOS (Peng-Robinson, Soave-Redlich-Kwong, Redlich-Kwong-Peng-Robinson) and Exact EOS (Helmholtz Energy functional) for a large number of real fluids, e.g. n-Dodecane, Methane, and water. 