#include "cubic_eos.h"

#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>

#include "my_macro.h"
#include "global_function.h"

namespace CubicEOS
{

void PureMaterial::Initialization(EOSType eos_type_param, const Species &species_param)
{

    strcpy(species_.name, species_param.name);
    strcpy(species_.molecular_formula, species_param.molecular_formula);
    species_.mass_molar = species_param.mass_molar;
    species_.omega = species_param.omega;
    species_.p_c = species_param.p_c;
    species_.T_c = species_param.T_c;
    species_.rho_c_molar = species_param.rho_c_molar;
    species_.volume_c_molar = 1.0 / species_.rho_c_molar;
    species_.Z_c = species_.p_c * species_.volume_c_molar / R_UNIVERSAL / species_.T_c;
    Loop1D(i_NASA, 0, 9) species_.coefficients_NASA[i_NASA] = species_param.coefficients_NASA[i_NASA];

    eos_type_ = eos_type_param;

    switch (eos_type_)
    {
    case PR:
        std::cout << std::endl;
        std::cout << "---------------------------------------------------------" << std::endl;
        std::cout << "          The peng-robinson eos will be used!            " << std::endl;
        std::cout << "---------------------------------------------------------" << std::endl;
        std::cout << std::endl;
        u_eos_ = 2.0;
        w_eos_ = -1.0;
        delta1_ = 1.0 + sqrt(2.0);
        delta2_ = 1.0 - sqrt(2.0);
        a_ = get_a_PR(species_.T_c, species_.p_c);
        b_ = get_b_PR(species_.T_c, species_.p_c);
        c0_ = get_c0_PR(species_.omega);
        p_GetAlphaAndDerivative = &PureMaterial::GetAlphaAndDerivative_PRandSRK;
        break;

    case SRK:
        std::cout << std::endl;
        std::cout << "---------------------------------------------------------" << std::endl;
        std::cout << "        The soave-redlich-kwong eos will be used!        " << std::endl;
        std::cout << "---------------------------------------------------------" << std::endl;
        std::cout << std::endl;
        u_eos_ = 1.0;
        w_eos_ = 0.0;
        delta1_ = 1.0;
        delta2_ = 0.0;
        a_ = get_a_SRK(species_.T_c, species_.p_c);
        b_ = get_b_SRK(species_.T_c, species_.p_c);
        c0_ = get_c0_SRK(species_.omega);
        p_GetAlphaAndDerivative = &PureMaterial::GetAlphaAndDerivative_PRandSRK;
        break;

    case RKPR:
        std::cout << std::endl;
        std::cout << "---------------------------------------------------------" << std::endl;
        std::cout << "     The redlich-kwong-peng-robinson eos will be used!   " << std::endl;
        std::cout << "---------------------------------------------------------" << std::endl;
        std::cout << std::endl;
        delta1_ = get_delta1_RKPR(species_.Z_c);
        delta2_ = (1 - delta1_) / (1 + delta1_);
        u_eos_ = delta1_ + delta2_;
        w_eos_ = delta1_ * delta2_;
        a_ = get_a_RKPR(species_.T_c, species_.p_c);
        b_ = get_b_RKPR(species_.T_c, species_.p_c);
        c0_ = get_c0_RKPR(species_.Z_c, species_.omega);
        p_GetAlphaAndDerivative = &PureMaterial::GetAlphaAndDerivative_RKPR;
        break;

    default:
        break;
    }

    return;
}

double PureMaterial::get_a_PR(double T_c, double p_c)
{
    double a_PR = 0.457236 * R_UNIVERSAL * R_UNIVERSAL * T_c * T_c / p_c;

    return a_PR;
}

double PureMaterial::get_b_PR(double T_c, double p_c)
{
    double b_PR = 0.077796 * R_UNIVERSAL * T_c / p_c;

    return b_PR;
}

double PureMaterial::get_c0_PR(double omega)
{
    double c0_PR = 0.37464 + 1.54226 * omega - 0.26992 * omega * omega;

    return c0_PR;
}

double PureMaterial::get_a_SRK(double T_c, double p_c)
{
    double a_SRK = 0.42748 * R_UNIVERSAL * R_UNIVERSAL * T_c * T_c / p_c;

    return a_SRK;
}

double PureMaterial::get_b_SRK(double T_c, double p_c)
{
    double b_SRK = 0.08664 * R_UNIVERSAL * T_c / p_c;

    return b_SRK;
}

double PureMaterial::get_c0_SRK(double omega)
{
    double c0_SRK = 0.48508 + 1.55171 * omega - 0.15613 * omega * omega;

    return c0_SRK;
}

double PureMaterial::get_delta1_RKPR(double Z_c)
{

    const double d_RKPR[6] = {0.42836300, 18.4962150, 0.33842600,
                              0.66000000, 789.723105, 2.51239200};

    double delta1 = d_RKPR[0] +
                    d_RKPR[1] * pow(fabs(d_RKPR[2] - 1.168 * Z_c), d_RKPR[3]) +
                    d_RKPR[4] * pow(fabs(d_RKPR[2] - 1.168 * Z_c), d_RKPR[5]);

    return delta1;
}

double PureMaterial::get_a_RKPR(double T_c, double p_c)
{

    double d = (1.0 + delta1_ * delta1_) / (1.0 + delta1_);

    double y = 1.0 + pow((2.0 * (1.0 + delta1_)), 1.0 / 3.0) +
               pow((4.0 / (1.0 + delta1_)), 1.0 / 3.0);

    double left = (3.0 * y * y + 3.0 * y * d + d * d + d - 1.0) /
                  (3.0 * y + d - 1.0) / (3.0 * y + d - 1.0);
    double a_RKPR = left * R_UNIVERSAL * R_UNIVERSAL * T_c * T_c / p_c;

    return a_RKPR;
}

double PureMaterial::get_b_RKPR(double T_c, double p_c)
{

    double d = (1.0 + delta1_ * delta1_) / (1.0 + delta1_);

    double y = 1.0 + pow((2.0 * (1.0 + delta1_)), 1.0 / 3.0) +
               pow((4.0 / (1.0 + delta1_)), 1.0 / 3.0);
    double left = 1.0 / (3.0 * y + d - 1.0);
    double b_RKPR = left * R_UNIVERSAL * T_c / p_c;

    return b_RKPR;
}

double PureMaterial::get_c0_RKPR(double Z_c, double omega)
{
    const double A0_RKPR = 0.0017;
    const double A1_RKPR = -2.4407;
    const double B0_RKPR = 1.9681;
    const double B1_RKPR = 7.4513;
    const double C0_RKPR = -2.7238;
    const double C1_RKPR = 12.5040;

    double c0_RKPR = (1.168 * Z_c * A1_RKPR + A0_RKPR) * omega * omega +
                     (1.168 * Z_c * B1_RKPR + B0_RKPR) * omega +
                     (1.168 * Z_c * C1_RKPR + C0_RKPR);
    return c0_RKPR;
}

void PureMaterial::GetAlphaAndDerivative(double T, double T_c, double *alpha_T, double *dalpha_dT, double *ddalpha_dT)
{
    (this->*p_GetAlphaAndDerivative)(T, T_c, alpha_T, dalpha_dT, ddalpha_dT);
    return;
}

void PureMaterial::GetAlphaAndDerivative_PRandSRK(double T, double T_c, double *alpha_T, double *dalpha_dT, double *ddalpha_dT)
{
    double tmp = 1.0 + c0_ * (1.0 - sqrt(T / T_c));
    *alpha_T = tmp * tmp;
    *dalpha_dT = -c0_ * tmp / sqrt(T_c * T);
    *ddalpha_dT = c0_ * (c0_ + 1.0) / (2.0 * sqrt(T * T * T * T_c));

    return;
}

void PureMaterial::GetAlphaAndDerivative_RKPR(double T, double T_c, double *alpha_T, double *dalpha_dT, double *ddalpha_dT)
{

    double denominator = 2.0 + T / T_c;

    double tmp = 3.0 / denominator;
    *alpha_T = pow(tmp, c0_);
    *dalpha_dT = -c0_ / T_c / 3.0 * pow(tmp, c0_ + 1.0);
    *ddalpha_dT = c0_ * (c0_ + 1.0) / T_c / T_c / 9.0 * pow(tmp, c0_ + 2.0);

    return;
}

double PureMaterial::GetPressureFromTemperatureAndVolume(double T, double volume)
{
    double denominator = volume * volume + u_eos_ * b_ * volume + w_eos_ * b_ * b_;

    double alpha_T;
    double dalpha_dT;
    double ddalpha_dT;
    GetAlphaAndDerivative(T, species_.T_c, &alpha_T, &dalpha_dT, &ddalpha_dT);

    double p = R_UNIVERSAL * T / (volume - b_) - a_ * alpha_T / denominator;

    return p;
}

double PureMaterial::GetVolumeFromTemperatureAndPressure(double T, double p, VolumeRootMode rootmode, char *phase)
{

    double alpha_T;
    double dalpha_dT;
    double ddalpha_dT;
    GetAlphaAndDerivative(T, species_.T_c, &alpha_T, &dalpha_dT, &ddalpha_dT);

    double A = (a_ * alpha_T * p) / (R_UNIVERSAL * T) / (R_UNIVERSAL * T);
    double B = (b_ * p) / (R_UNIVERSAL * T);
    double uEOSwEOS = sqrt(u_eos_ * u_eos_ - 4 * w_eos_);

    double a2 = (B * (u_eos_ - 1.0) - 1.0);
    double a1 = (A + B * (w_eos_ * B - u_eos_ * B - u_eos_));
    double a0 = -B * (w_eos_ * B * B + w_eos_ * B + A);

    double Q = (a2 * a2 - 3 * a1) / 9.0;
    double R = (2 * a2 * a2 * a2 - 9 * a2 * a1 + 27 * a0) / 54.0;

    double z1 = 0.0, z2 = 0.0, z3 = 0.0;
    double zmin = 0.0, zmax = 0.0, z_root = 0.0;

    if (R * R < Q * Q * Q)
    {
        double phi = acos(R / sqrt(Q * Q * Q));
        z1 = -2.0 * sqrt(Q) * cos(phi / 3.0) - a2 / 3.0;
        z2 = -2.0 * sqrt(Q) * cos(phi / 3.0 + 2 * Pi / 3.0) - a2 / 3.0;
        z3 = -2.0 * sqrt(Q) * cos(phi / 3.0 - 2 * Pi / 3.0) - a2 / 3.0;

        zmin = std::min(std::min(z1, z2), z3);
        zmax = std::max(std::max(z1, z2), z3);

        if (zmin < B)
        {

            z_root = zmax;

            if (z_root < 3.5 * B)
                strcpy(phase, "liquid");
            else
                strcpy(phase, "vapor");
        }
        else
        {

            switch (rootmode)
            {
            case kLiquid:
                z_root = zmin;
                strcpy(phase, "liquid");
                break;

            case kVapor:
                z_root = zmax;
                strcpy(phase, "vapor");
                break;

            case kGibbs:

                double tmp1 = B * (u_eos_ - uEOSwEOS);
                double tmp2 = B * (u_eos_ + uEOSwEOS);
                double z_liq = zmin;
                double z_vap = zmax;
                double lamda_liq_p = 2 * z_liq + tmp2;
                double lamda_liq_m = 2 * z_liq + tmp1;
                double lamda_vap_p = 2 * z_vap + tmp2;
                double lamda_vap_m = 2 * z_vap + tmp1;

                double delta_gibbs = log((z_liq - B) / (z_vap - B)) +
                                     A / (B * uEOSwEOS) * log(lamda_liq_p * lamda_vap_m / lamda_liq_m / lamda_vap_p) -
                                     (z_liq - z_vap);
                if (delta_gibbs < 0.0)
                {
                    z_root = z_vap;
                    strcpy(phase, "vapor");
                }
                else
                {
                    z_root = z_liq;
                    strcpy(phase, "liquid");
                }
                break;

            default:
                break;
            }
        }
    }
    else
    {
        double C1 = 0.0;
        double C2 = 0.0;

        C1 = -Sign(1.0, R) * pow((fabs(R) + sqrt(R * R - Q * Q * Q)), 1.0 / 3.0);
        if (C1 != 0.0)
            C2 = Q / C1;

        z_root = C1 + C2 - a2 / 3.0;
        if (z_root < 3.5 * B)
            strcpy(phase, "liquid");
        else
            strcpy(phase, "vapor");
    }
    double volume = z_root * R_UNIVERSAL * T / p;

    return volume;
}

double PureMaterial::get_dpdT_v(double T, double volume)
{
    double alpha_T;
    double dalpha_dT;
    double ddalpha_dT;
    GetAlphaAndDerivative(T, species_.T_c, &alpha_T, &dalpha_dT, &ddalpha_dT);
    double denominator = volume * volume + u_eos_ * b_ * volume + w_eos_ * b_ * b_;
    double dpdT_v = R_UNIVERSAL / (volume - b_) - a_ * dalpha_dT / denominator;

    return dpdT_v;
}

double PureMaterial::get_dpdv_T(double T, double volume)
{
    double alpha_T;
    double dalpha_dT;
    double ddalpha_dT;
    GetAlphaAndDerivative(T, species_.T_c, &alpha_T, &dalpha_dT, &ddalpha_dT);
    double denominator = volume * volume + u_eos_ * b_ * volume + w_eos_ * b_ * b_;
    double dpdv_T = (2. * volume + u_eos_ * b_) * a_ * alpha_T / denominator / denominator -
                    R_UNIVERSAL * T / (volume - b_) / (volume - b_);

    return dpdv_T;
}

double PureMaterial::GetReferenceNASA(double T, const double coefficients[9], CaloricProperty which_property)
{
    double lnT = log(T);

    double cof[9];
    cof[0] = coefficients[0] / T / T;
    cof[1] = coefficients[1] / T;
    cof[2] = coefficients[2];
    cof[3] = coefficients[3] * T;
    cof[4] = coefficients[4] * T * T;
    cof[5] = coefficients[5] * T * T * T;
    cof[6] = coefficients[6] * T * T * T * T;
    cof[7] = coefficients[7] / T;
    cof[8] = coefficients[8];

    int i_cp = 0;
    int i_h = 1;
    int i_s = 2;
    int i_g = 3;

    double caloric_cof[4][9] = {
        {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0},
        {-1.0, lnT, 1.0, 0.5, 1.0 / 3.0, 0.25, 0.2, 1.0, 0.0},
        {-0.5, -1.0, lnT, 1.0, 0.5, 1.0 / 3.0, 0.25, 0.0, 1.0},
        {-0.5, (lnT + 1.0), (1.0 - lnT), -0.5, -1.0 / 6.0, -1.0 / 12.0, -0.05, 1.0, -1.0}};

    double caloric_property_ideal = 0.0;
    switch (which_property)
    {
    case kInternalEnergy:

        Loop1D(j_cof, 0, 8) caloric_property_ideal += cof[j_cof] * caloric_cof[i_h][j_cof];

        caloric_property_ideal *= (R_UNIVERSAL * T);

        caloric_property_ideal -= (R_UNIVERSAL * T);
        break;

    case kEnthalpy:

        Loop1D(j_cof, 0, 9) caloric_property_ideal += cof[j_cof] * caloric_cof[i_h][j_cof];

        caloric_property_ideal *= (R_UNIVERSAL * T);
        break;

    case kEntropy:

        Loop1D(j_cof, 0, 9) caloric_property_ideal += cof[j_cof] * caloric_cof[i_s][j_cof];

        caloric_property_ideal *= R_UNIVERSAL;
        break;

    case kGibbsEnergy:

        Loop1D(j_cof, 0, 9) caloric_property_ideal += cof[j_cof] * caloric_cof[i_g][j_cof];

        caloric_property_ideal *= (R_UNIVERSAL * T);
        break;

    case kHeatCapacityV:

        Loop1D(j_cof, 0, 9) caloric_property_ideal += cof[j_cof] * caloric_cof[i_cp][j_cof];

        caloric_property_ideal *= R_UNIVERSAL;

        caloric_property_ideal -= R_UNIVERSAL;
        break;

    case kHeatCapacityP:

        Loop1D(j_cof, 0, 9) caloric_property_ideal += cof[j_cof] * caloric_cof[i_cp][j_cof];

        caloric_property_ideal *= R_UNIVERSAL;
        break;

    default:
        break;
    }

    return caloric_property_ideal;
}

double PureMaterial::GetDepartureFunction(double T, double p, double volume, CaloricProperty which_property)
{
    double alpha_T;
    double dalpha_dT;
    double ddalpha_dT;
    GetAlphaAndDerivative(T, species_.T_c, &alpha_T, &dalpha_dT, &ddalpha_dT);

    double uEOSwEOS = sqrt(u_eos_ * u_eos_ - 4 * w_eos_);
    double lamada_p = 2 * volume + b_ * (u_eos_ + uEOSwEOS);
    double lamada_m = 2 * volume + b_ * (u_eos_ - uEOSwEOS);
    double K = 1.0 / (b_ * uEOSwEOS) * log(lamada_m / lamada_p);

    double departure_func = 0.0;

    double de = (a_ * alpha_T - T * a_ * dalpha_dT) * K;
    double dh = de + p * volume - R_UNIVERSAL * T;
    double ds = -a_ * dalpha_dT * K + R_UNIVERSAL * log(1 - b_ / volume);
    double dg = dh - T * ds;
    double dcv = -T * a_ * ddalpha_dT * K;

    double dpdv_T = get_dpdv_T(T, volume);
    double dpdT_v = get_dpdT_v(T, volume);

    double dcp = -R_UNIVERSAL + dcv - T * (dpdT_v) * (dpdT_v) / dpdv_T;

    switch (which_property)
    {
    case kInternalEnergy:
        departure_func = de;
        break;

    case kEnthalpy:
        departure_func = dh;
        break;

    case kEntropy:
        departure_func = ds;
        break;

    case kGibbsEnergy:
        departure_func = dg;
        break;

    case kHeatCapacityV:
        departure_func = dcv;
        break;

    case kHeatCapacityP:
        departure_func = dcp;
        break;

    default:
        break;
    }

    return departure_func;
}

double PureMaterial::GetCaloricProperty(double T, double p, double volume, CaloricProperty which_property)
{
    double reference = GetReferenceNASA(T, species_.coefficients_NASA, which_property);
    double departure_func = GetDepartureFunction(T, p, volume, which_property);
    double caloric_property = reference + departure_func;

    return caloric_property;
}

double PureMaterial::GetExpansivity(double T, double volume)
{
    double dpdT_v = get_dpdT_v(T, volume);
    double dpdv_T = get_dpdv_T(T, volume);

    double alpha_p = -1.0 / volume * dpdT_v / dpdv_T;
}

double PureMaterial::GetCompressibility(double T, double volume)
{
    double dpdv_T = get_dpdv_T(T, volume);

    double beta_T = -1.0 / volume / dpdv_T;
}

double PureMaterial::GetSpeedOfSound(double T, double p, double volume)
{
    double cp = GetCaloricProperty(T, p, volume, kHeatCapacityP);
    double cv = GetCaloricProperty(T, p, volume, kHeatCapacityV);
    double dpdv_T = get_dpdv_T(T, volume);

    double mass_molar = species_.mass_molar;
    double c = sqrt(-cp / cv * dpdv_T * volume * volume / mass_molar);

    return c;
}

double PureMaterial::GetTemperatureFromVolumeAndInternalEnergy(double volume, double e, double T_guess)
{
    const int iter_max = 20;
    int iter = 0;

    const double threshold = 1.0e-6;
    const double relax_factor = 1.0e-5;
    const double T_min = 50.0;
    const double T_max = 4000.0;
    double p_iter = 0.0;
    double T_iter = 0.0;
    double e_iter = 0.0;
    double residual = 0.0;

    T_iter = T_guess;
    do
    {

        if (iter > iter_max)
        {
            std::cout << "Newtown iteration for temperature failled@!" << std::endl;
            exit(0);
        }

        if (T_iter < T_min || T_iter > T_max)
        {
            std::cout << "Temperature is beyond the boundary during the newton iteration!" << std::endl;
            exit(0);
        }

        if (std::isnan(T_iter) || std::isinf(T_iter))
        {
            std::cout << "Some terribly bad things happened in the process of newton iteration!" << std::endl;
            std::cout << "The value of emperature is not a number!" << std::endl;
            exit(0);
        }
        ++iter;
        e_iter = GetCaloricProperty(T_iter, p_iter, volume, kInternalEnergy);

        double e_norm = fabs(e) > 1.0 ? e : 1.0;
        double de = e - e_iter;

        residual = fabs(de / e_norm);

        double cv = GetCaloricProperty(T_iter, p_iter, volume, kHeatCapacityV);
        double dT = de / cv;
        T_iter = T_iter + dT / (1 + fabs(relax_factor * dT));

    } while (residual > threshold);

    std::cout << "Converge after " << iter << " times iterations! " << std::endl;

    return T_iter;
}

double PureMaterial::GetTemperatureFromVolumeAndPressure(double volume, double p, double T_guess)
{
    const int iter_max = 20;
    int iter = 0;

    const double threshold = 1.0e-6;
    const double relax_factor = 1.0e-5;
    const double T_min = 50.0;
    const double T_max = 4000.0;
    double p_iter = 0.0;
    double T_iter = 0.0;
    double e_iter = 0.0;
    double residual = 0.0;

    T_iter = T_guess;
    do
    {

        if (iter > iter_max)
        {
            std::cout << "Newtown iteration for temperature failled!" << std::endl;
            exit(0);
        }

        if (T_iter < T_min || T_iter > T_max)
        {
            std::cout << "Temperature is beyond the boundary during the newton iteration!" << std::endl;
            exit(0);
        }

        if (std::isnan(T_iter) || std::isinf(T_iter))
        {
            std::cout << "Some terribly bad things happened in the process of newton iteration!" << std::endl;
            std::cout << "The value of emperature is not a number!" << std::endl;
            exit(0);
        }
        ++iter;
        p_iter = GetPressureFromTemperatureAndVolume(T_iter, volume);

        double p_norm = fabs(p) > 1.0 ? p : 1.0;
        double dp = p - p_iter;

        residual = fabs(dp / p_norm);

        double dpdT_v = get_dpdT_v(T_iter, volume);
        double dT = dp / dpdT_v;
        T_iter = T_iter + dT / (1 + fabs(relax_factor * dT));

    } while (residual > threshold);

    std::cout << "Converge after " << iter << " times iterations! " << std::endl;

    return T_iter;
}

double PureMaterial::GetThermoFromTemperatureAndPressure(double T, double p, ThermodynamicProperty which_property)
{

    double thermo_property = 0.0;
    char phase[20];
    double volume = GetVolumeFromTemperatureAndPressure(T, p, kGibbs, phase);

    switch (which_property)
    {
    case TEMPERATURE:
        std::cout << "WARNING!!!" << std::endl;
        std::cout << "ALREADY KNOWN PROPERTY: TEMPERATURE" << std::endl;
        thermo_property = T;
        break;

    case PRESSURE:
        std::cout << "WARNING!!!" << std::endl;
        std::cout << "ALREADY KNOWN PROPERTY: PRESSURE" << std::endl;
        thermo_property = p;
        break;

    case VOLUME:
        thermo_property = volume;
        break;

    case INTERNAL_ENERGY:
        thermo_property = GetCaloricProperty(T, p, volume, kInternalEnergy);
        break;

    case ENTHALPY:
        thermo_property = GetCaloricProperty(T, p, volume, kEnthalpy);
        break;

    case ENTROPY:
        thermo_property = GetCaloricProperty(T, p, volume, kEntropy);
        break;

    case GIBBS_ENERGY:
        thermo_property = GetCaloricProperty(T, p, volume, kGibbsEnergy);
        break;

    case HEATCAPACITY_V:
        thermo_property = GetCaloricProperty(T, p, volume, kHeatCapacityV);
        break;

    case HEATCAPACITY_P:
        thermo_property = GetCaloricProperty(T, p, volume, kHeatCapacityP);
        break;

    case SPECIFIC_HEAT_RATIO:
        thermo_property = GetCaloricProperty(T, p, volume, kHeatCapacityP) /
                          GetCaloricProperty(T, p, volume, kHeatCapacityV);
        break;

    case SPEED_OF_SOUND:
        thermo_property = GetSpeedOfSound(T, p, volume);
        break;

    default:
        break;
    }

    return thermo_property;
}

double PureMaterial::GetThermoFromTemperatureAndVolume(double T, double volume, ThermodynamicProperty which_property)
{

    double thermo_property = 0.0;
    double p = GetPressureFromTemperatureAndVolume(T, volume);

    switch (which_property)
    {
    case TEMPERATURE:
        std::cout << "WARNING!!!" << std::endl;
        std::cout << "ALREADY KNOWN PROPERTY: TEMPERATURE" << std::endl;
        thermo_property = T;
        break;

    case PRESSURE:
        thermo_property = p;
        break;

    case VOLUME:
        std::cout << "WARNING!!!" << std::endl;
        std::cout << "ALREADY KNOWN PROPERTY: VOLUME" << std::endl;
        thermo_property = volume;
        break;

    case INTERNAL_ENERGY:
        thermo_property = GetCaloricProperty(T, p, volume, kInternalEnergy);
        break;

    case ENTHALPY:
        thermo_property = GetCaloricProperty(T, p, volume, kEnthalpy);
        break;

    case ENTROPY:
        thermo_property = GetCaloricProperty(T, p, volume, kEntropy);
        break;

    case GIBBS_ENERGY:
        thermo_property = GetCaloricProperty(T, p, volume, kGibbsEnergy);
        break;

    case HEATCAPACITY_V:
        thermo_property = GetCaloricProperty(T, p, volume, kHeatCapacityV);
        break;

    case HEATCAPACITY_P:
        thermo_property = GetCaloricProperty(T, p, volume, kHeatCapacityP);
        break;

    case SPECIFIC_HEAT_RATIO:
        thermo_property = GetCaloricProperty(T, p, volume, kHeatCapacityP) /
                          GetCaloricProperty(T, p, volume, kHeatCapacityV);
        break;

    case SPEED_OF_SOUND:
        thermo_property = GetSpeedOfSound(T, p, volume);
        break;

    default:
        break;
    }

    return thermo_property;
}

double PureMaterial::GetThermoFromVolumeAndInternalEnergy(double volume, double e, double T_guess, ThermodynamicProperty which_property)
{
    double T = GetTemperatureFromVolumeAndInternalEnergy(volume, e, T_guess);

    double thermo_property = 0.0;
    double p = GetPressureFromTemperatureAndVolume(T, volume);

    switch (which_property)
    {
    case TEMPERATURE:
        thermo_property = T;
        break;

    case PRESSURE:
        thermo_property = p;
        break;

    case VOLUME:
        std::cout << "WARNING!!!" << std::endl;
        std::cout << "ALREADY KNOWN PROPERTY: VOLUME" << std::endl;
        thermo_property = volume;
        break;

    case INTERNAL_ENERGY:
        std::cout << "WARNING!!!" << std::endl;
        std::cout << "ALREADY KNOWN PROPERTY: INTERNAL_ENERGY" << std::endl;
        thermo_property = e;
        break;

    case ENTHALPY:
        thermo_property = GetCaloricProperty(T, p, volume, kEnthalpy);
        break;

    case ENTROPY:
        thermo_property = GetCaloricProperty(T, p, volume, kEntropy);
        break;

    case GIBBS_ENERGY:
        thermo_property = GetCaloricProperty(T, p, volume, kGibbsEnergy);
        break;

    case HEATCAPACITY_V:
        thermo_property = GetCaloricProperty(T, p, volume, kHeatCapacityV);
        break;

    case HEATCAPACITY_P:
        thermo_property = GetCaloricProperty(T, p, volume, kHeatCapacityP);
        break;

    case SPECIFIC_HEAT_RATIO:
        thermo_property = GetCaloricProperty(T, p, volume, kHeatCapacityP) /
                          GetCaloricProperty(T, p, volume, kHeatCapacityV);
        break;

    case SPEED_OF_SOUND:
        thermo_property = GetSpeedOfSound(T, p, volume);
        break;

    default:
        break;
    }

    return thermo_property;
}

double PureMaterial::get_dpdv_e(double T, double p, double volume)
{
    double dpdT_v = get_dpdT_v(T, volume);
    double dpdv_T = get_dpdv_T(T, volume);

    double dvdT_p = -dpdT_v / dpdv_T;
    double cp = GetCaloricProperty(T, p, volume, kHeatCapacityP);
    double cv = GetCaloricProperty(T, p, volume, kHeatCapacityV);

    double dpdv_e = -dpdT_v / cv * (cp / dvdT_p - p);

    return dpdv_e;
}

double PureMaterial::get_dpde_v(double T, double p, double volume)
{
    double dpdT_v = get_dpdT_v(T, volume);
    double cv = GetCaloricProperty(T, p, volume, kHeatCapacityV);

    double dpde_v = 1.0 / cv * dpdT_v;

    return dpde_v;
}

} // namespace CubicEOS