
#include "mixed_cubiceos.h"

#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>

#include "my_macro.h"

namespace CubicEOS
{

void MixedMaterial::Initialization(const EOSType eos_type_param, const int num_of_species_param,
                                   const Species *list_species_param)
{
    
    num_of_species_ = num_of_species_param;
    list_species_ = new Species[num_of_species_];
    list_a_ = new double[num_of_species_];
    list_b_ = new double[num_of_species_];
    list_c0_ = new double[num_of_species_];

    Loop1D(i, 0, num_of_species_)
    {
        strcpy(list_species_[i].name, list_species_param[i].name);
        strcpy(list_species_[i].molecular_formula, list_species_param[i].molecular_formula);
        list_species_[i].mass_molar = list_species_param[i].mass_molar;
        list_species_[i].omega = list_species_param[i].omega;
        list_species_[i].p_c = list_species_param[i].p_c;
        list_species_[i].T_c = list_species_param[i].T_c;
        list_species_[i].rho_c_molar = list_species_param[i].rho_c_molar;
        list_species_[i].volume_c_molar = 1.0 / list_species_[i].rho_c_molar;
        list_species_[i].Z_c = list_species_[i].p_c * list_species_[i].volume_c_molar / R_UNIVERSAL /
                               list_species_[i].T_c;
    }

    
    eos_type_ = eos_type_param;

    switch (eos_type_)
    {
    case PR:
        std::cout << std::endl;
        std::cout << "---------------------------------------------------------" << std::endl;
        std::cout << "          The peng-robinson eos will be used !           " << std::endl;
        std::cout << "---------------------------------------------------------" << std::endl;
        std::cout << std::endl;
        
        u_eos_mixed_ = 2.0;
        w_eos_mixed_ = 1.0;
        delta1_mixed_ = 1.0 + sqrt(2.0);
        delta2_mixed_ = 1.0 - sqrt(2.0);
        get_a_PR_list();
        get_b_PR_list();
        get_c0_PR_list();
        p_GetAlphaAndDerivativeList = &MixedMaterial::GetAlphaAndDerivative_PRandSRK_List;
        break;

    case SRK:
        std::cout << std::endl;
        std::cout << "---------------------------------------------------------" << std::endl;
        std::cout << "        The soave-redlich-kwong eos will be used!        " << std::endl;
        std::cout << "---------------------------------------------------------" << std::endl;
        std::cout << std::endl;
        
        u_eos_mixed_ = 1.0;
        w_eos_mixed_ = 0.0;
        delta1_mixed_ = 1.0;
        delta2_mixed_ = 0.0;
        get_a_SRK_list();
        get_b_SRK_list();
        get_c0_SRK_list();
        p_GetAlphaAndDerivativeList = &MixedMaterial::GetAlphaAndDerivative_PRandSRK_List;
        break;

    case RKPR:
        std::cout << std::endl;
        std::cout << "---------------------------------------------------------" << std::endl;
        std::cout << "     The redlich-kwong-peng-robinson eos will be used!   " << std::endl;
        std::cout << "---------------------------------------------------------" << std::endl;
        std::cout << std::endl;
        list_delta1_ = new double[num_of_species_];
        list_delta2_ = new double[num_of_species_];
        get_delta12_RKPR_list();
        
        get_a_RKPR_list();
        get_b_RKPR_list();
        get_c0_RKPR_list();
        p_GetAlphaAndDerivativeList = &MixedMaterial::GetAlphaAndDerivative_RKPR_List;
        break;

    default:
        break;
    }

    return;
}

void MixedMaterial::ReleaseMemory()
{
    delete[] list_species_;
    delete[] list_a_;
    delete[] list_b_;
    delete[] list_c0_;

    return;
}

void MixedMaterial::get_a_PR_list()
{
    Loop1D(i, 0, num_of_species_)
    {
        list_a_[i] = 0.457236 * R_UNIVERSAL * R_UNIVERSAL *
                     list_species_[i].T_c * list_species_[i].T_c / list_species_[i].p_c;
    }

    return;
}

void MixedMaterial::get_b_PR_list()
{
    Loop1D(i, 0, num_of_species_)
    {
        list_b_[i] = 0.077796 * R_UNIVERSAL * list_species_[i].T_c / list_species_[i].p_c;
    }

    return;
}

void MixedMaterial::get_c0_PR_list()
{
    Loop1D(i, 0, num_of_species_)
    {
        list_c0_[i] = 0.37464 + 1.54226 * list_species_[i].omega -
                      0.26992 * list_species_[i].omega * list_species_[i].omega;
    }

    return;
}

void MixedMaterial::get_a_SRK_list()
{
    Loop1D(i, 0, num_of_species_)
    {
        list_a_[i] = 0.42748 * R_UNIVERSAL * R_UNIVERSAL * list_species_[i].T_c *
                       list_species_[i].T_c / list_species_[i].p_c;
    }

    return;
}

void MixedMaterial::get_b_SRK_list()
{
    Loop1D(i,0, num_of_species_)
    {
        list_b_[i] = 0.08664 * R_UNIVERSAL * list_species_[i].T_c / list_species_[i].p_c;
    }

    return;
}

void MixedMaterial::get_c0_SRK_list()
{
    Loop1D(i, 0, num_of_species_)
    {
        list_c0_[i] = 0.48508 + 1.55171 * list_species_[i].omega -
                      0.15613 * list_species_[i].omega * list_species_[i].omega; 
        
        
    }

    return;
}

void MixedMaterial::get_delta12_RKPR_list()
{
    
    const double d_RKPR[6] = {0.42836300, 18.4962150, 0.33842600,
                              0.66000000, 789.723105, 2.51239200};

    Loop1D(i, 0, num_of_species_)
    {

        list_delta1_[i] = d_RKPR[0] +
                          d_RKPR[1] * pow(fabs(d_RKPR[2] - 1.168 * list_species_[i].Z_c), d_RKPR[3]) +
                          d_RKPR[4] * pow(fabs(d_RKPR[2] - 1.168 * list_species_[i].Z_c), d_RKPR[5]);

        list_delta2_[i] = (1 - list_delta1_[i]) / (1 + list_delta1_[i]);
    }

    return;
}

void MixedMaterial::get_a_RKPR_list()
{
    double d, y, left;
    Loop1D(i, 0, num_of_species_)
    {
        
        d = (1.0 + list_delta1_[i] * list_delta1_[i]) / (1.0 + list_delta1_[i]);
        y = 1.0 + pow((2.0 * (1.0 + list_delta1_[i])), 1.0 / 3.0) +
            pow((4.0 / (1.0 + list_delta1_[i])), 1.0 / 3.0);
        left = (3.0 * y * y + 3.0 * y * d + d * d + d - 1.0) /
               (3.0 * y + d - 1.0) / (3.0 * y + d - 1.0);
        list_a_[i] = left * R_UNIVERSAL * R_UNIVERSAL * list_species_[i].T_c *
                     list_species_[i].T_c / list_species_[i].p_c;
    }

    return;
}

void MixedMaterial::get_b_RKPR_list()
{
    double d, y, left;
    Loop1D(i, 0, num_of_species_)
    {
        
        double d = (1.0 + list_delta1_[i] * list_delta1_[i]) / (1.0 + list_delta1_[i]);
        double y = 1.0 + pow((2.0 * (1.0 + list_delta1_[i])), 1.0 / 3.0) +
                   pow((4.0 / (1.0 + list_delta1_[i])), 1.0 / 3.0);
        double left = 1.0 / (3.0 * y + d - 1.0);
        double b_RKPR = left * R_UNIVERSAL * list_species_[i].T_c / list_species_[i].p_c;
    }

    return;
}

void MixedMaterial::get_c0_RKPR_list()
{
    const double A0_RKPR = 0.0017;
    const double A1_RKPR = -2.4407;
    const double B0_RKPR = 1.9681;
    const double B1_RKPR = 7.4513;
    const double C0_RKPR = -2.7238;
    const double C1_RKPR = 12.5040;

    Loop1D(i,0,num_of_species_)
    {
        list_c0_[i] = (1.168 * list_species_[i].Z_c * A1_RKPR + A0_RKPR) * list_species_[i].omega * list_species_[i].omega +
                     (1.168 * list_species_[i].Z_c * B1_RKPR + B0_RKPR) * list_species_[i].omega +
                     (1.168 * list_species_[i].Z_c * C1_RKPR + C0_RKPR);
    }
    return;
}

void MixedMaterial::GetAlphaAndDerivative_PRandSRK_List(double T, double *list_alpha_T, double *list_dalpha_dT, double *list_ddalpha_dT)
{
    double tmp;
    Loop1D(i, 0, num_of_species_)
    {
        tmp = 1.0 + list_c0_[i] * (1.0 - sqrt(T / list_species_[i].T_c)); 
        list_alpha_T[i] = tmp * tmp;
        list_dalpha_dT[i] = -list_c0_[i] * tmp / sqrt(list_species_[i].T_c * T);
        list_ddalpha_dT[i] = list_c0_[i] * (list_c0_[i] + 1.0) / (2.0 * sqrt(T * T * T * list_species_[i].T_c));
    }
    return;
}

void MixedMaterial::GetAlphaAndDerivative_RKPR_List(double T, double *list_alpha_T, double *list_dalpha_dT, double *list_ddalpha_dT)
{
    double denominator,tmp;
    Loop1D(i, 0, num_of_species_)
    {
        
        denominator = 2.0 + T / list_species_[i].T_c;

        tmp = 3.0 / denominator;
        list_alpha_T[i] = pow(tmp, list_c0_[i]);
        list_dalpha_dT[i] = -list_c0_[i] / list_species_[i].T_c / 3.0 * pow(tmp, list_c0_[i] + 1.0);
        list_ddalpha_dT[i] = list_c0_[i] * (list_c0_[i] + 1.0) / list_species_[i].T_c /
                             list_species_[i].T_c / 9.0 * pow(tmp, list_c0_[i] + 2.0);
    }
    return;
}

} // namespace CubicEOS