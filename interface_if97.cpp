#include "interface_if97.h"

double GetPressureFromDenAndTemp_IF97(double rho, double T, double p_guess)
{
    const int iter_max = 50;
    int iter = 0;

    const double threshold = 1.0e-6;
    const double relax_factor = 1.0e-5;
    const double p_min = 0.0;   
    const double p_max = 100.0e6; 
    double p_iter = 0.0;
    double rho_iter = 0.0;
    double residual = 0.0;

    
    p_iter = p_guess;
    do
    {
        
        
        if (iter > iter_max)
        {
            std::cout << "Newtown iteration for pressure failled!" << std::endl;
            exit(0);
            
        }
        
        if (p_iter < p_min || p_iter > p_max)
        {
            std::cout << "Pressure is beyond the boundary during the newton iteration!" << std::endl;
            exit(0);
        }
        
        if (std::isnan(p_iter) || std::isinf(p_iter))
        {
            std::cout << "Some terribly bad things happened in the process of newton iteration!" << std::endl;
            std::cout << "The value of pressure is not a number!" << std::endl;
            exit(0);
        }
        ++iter;
        rho_iter = rhomass_Tp(T, p_iter);

        double rho_norm = fabs(rho) > 1.0 ? rho : 1.0;
        double drho = rho - rho_iter;
        
        residual = fabs(drho / rho_norm);

        
        double drhodp_T = drhodp_Tp(T,p_iter);
        double dp = drho / drhodp_T; 
        p_iter = p_iter + dp / (1 + fabs(relax_factor * dp));

    } while (residual > threshold);

    std::cout << "Converge after " << iter << " times iterations! " << std::endl;

    return p_iter;

}

double GetInternalEnergyFromDenAndTemp_IF97(double rho, double T, double p_guess)
{
    double p = GetPressureFromDenAndTemp_IF97(rho, T, p_guess);
    double u = umass_Tp(T, p);

    return u;
}


double GetTempFromDenAndInternalEnergy_IF97(double rho, double e, double T_guess)
{
        const int iter_max = 50;
        int iter = 0;

        const double threshold = 1.0e-6;
        const double relax_factor = 1.0e-5;
        const double T_min = 273.15;   
        const double T_max = 2273.15; 
        double T_iter = 0.0;
        double e_iter = 0.0;
        double residual = 0.0;

        
        T_iter = T_guess;
        do
        {
            
            
            if (iter > iter_max)
            {
                std::cout << "Newtown iteration for temperature failled!" << std::endl;
                exit(0);
                
            }
            
            if (T_iter < T_min || T_iter > T_max)
            {
                std::cout << "Temperature is beyond the boundary during the newton iteration!" << std::endl;
                exit(0);
            }
            
            if (std::isnan(T_iter) || std::isinf(T_iter))
            {
                std::cout << "Some terribly bad things happened in the process of newton iteration!" << std::endl;
                std::cout << "The value of emperature is not a number!" << std::endl;
                exit(0);
            }
            ++iter;
            double p_test = psat97(T_iter);
            p_test *= 1.2;
            double p_iter = GetPressureFromDenAndTemp_IF97(rho, T_iter, p_test);
            e_iter = umass_Tp(T_iter,p_iter);

            double e_norm = fabs(e) > 1.0 ? e : 1.0;
            double de = e - e_iter;
            
            residual = fabs(de / e_norm);

            
            double cv = cvmass_Tp(T_iter,p_iter);
            double dT = de / cv; 
            T_iter = T_iter + dT / (1 + fabs(relax_factor * dT));

        } while (residual > threshold);

        std::cout << "Converge after " << iter << " times iterations! " << std::endl;

        return T_iter;
}